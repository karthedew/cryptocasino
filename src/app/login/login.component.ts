import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Router, RouterStateSnapshot } from "@angular/router";
import { Apollo } from "apollo-angular";
import { UserQueryGQL, User, CheckUserGQL } from "../services/graphql/UserQuery.service";
import { AuthQueryGQL } from "../services/graphql/AuthQuery.service";
import { GenerateUserGQL } from "../services/graphql/UserMutation.service";


// import { WEB3 } from "../services/web3";
import { WEB3 } from "../auth/services/web3.service";
import { Subscription, Observable, Subject } from 'rxjs';
import { map } from "rxjs/operators";


// const UserQueryGQL = gql`
//   query UserQueryGQL($publicAddress: String!) {
//     getUser(objects: {publickey: $publicAddress}) {
//       id
//       username
//       publickey
//       nonce
//     }
//   }
//   `;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  // animations: [moveIn()],
  // host: {'[@moveIn]':''}
})
export class LoginComponent implements OnInit, OnDestroy {

  loading: boolean = false;
  test: string = "This is the test login page.";
  login: boolean = true;

  address: string;
  nonce: number;
  error: boolean;

  searchSubject$ = new Subject<string>();
  results$: Observable<any>

  // private querySubscription: Subscription
  user: Observable<User>;
  newUser: Observable<any>;
  getCurrentUser: Observable<User>;

  // START HERE
  data: Observable<any>;
  checkuser: Observable<any>;
  isUser: boolean;
  signature: string;
  jwt: string;

  constructor(
    private router: Router,
    @Inject(WEB3) private web3: any,
    private userQueryGQL: UserQueryGQL,
    private checkUserGQL: CheckUserGQL,
    private authQueryGQL: AuthQueryGQL,
    private generateUserGQL: GenerateUserGQL,
    private apollo: Apollo
              ) {  }

  ngOnInit() {  }

  ngOnDestroy() {
    // this.querySubscription.unsubscribe();
  }


  async loginMetaMask() {
    try {
      if('enable' in this.web3.currentProvider) {
        await this.web3.currentProvider.enable();
      }
    } catch(err) {
      console.log('You have to install MetaMask extension');
      console.error(err);
    }
    // -------------------------------------------
    // STEP 1:
    // -------------------------------------------
    // --- Get Account & Sign-In with MetaMask ---
    const accounts = await this.web3.eth.getAccounts();
    this.address = accounts[0];

    // --- Get the query subscriptions ---
    this.checkuser = this.checkUserQuery()
    this.getCurrentUser = this.GetCurrentUser()

    // --- Do your authentication ---
    this.checkuser.pipe(
      map((res) => {
        this.isUser = res.data['checkUser']
      })
    )
      .subscribe(result => {
        // -------------------------------------------
        // STEP 2:
        // -------------------------------------------
        // --- Check Backend to See If User Exists ---
        // --- If user does not exist, create new user ---
        // --- If user does exist, continue ---
        if(this.isUser) {
          this.AuthenticateUser();
        } else {
          console.error("User not found!");
          // Login User
          this.SubmitNewUser();
          this.AuthenticateUser();
        }
        // this.router.navigate(['/'], { queryParams: { returnUrl: state.url } });
      });
    
    // --- Want to Handle the Front-End while backend is parsing ---
    this.loading = true;
    console.log('You made it!')
    console.log(this.loading)

    return
  }

  private checkUserQuery(): Observable<any> {
    return this.checkUserGQL.watch({
      publicAddress: this.address
    })
      .valueChanges
  }

  private GetCurrentUser(): Observable<any> {
    return this.userQueryGQL.watch({
      publicAddress: this.address
    })
      .valueChanges
  }

  private AuthQuery(): Observable<any> {
    return this.authQueryGQL.watch({
      publicAddress: this.address,
      signature: this.signature
    })
      .valueChanges
  }

  private async CreateSignature() {
    try {
      const signature = await this.web3.eth.personal.sign(
        `I am signing my one-time nonce: ${this.nonce}`,
        this.address,
        ''
      )
      return signature
    } catch (err) {
      console.log("You need to sign your nonce to get access!")
      console.log(err)
      return "ERROR"
    }
  }

  private SubmitNewUser() {

    const username = this.GetNewUsername();
    
    console.log('The new username is: ')
    console.log(username)

    this.apollo.mutate({
      mutation: this.generateUserGQL.document,
      variables: {
        username: username,
        address: this.address
      }
    })
    .subscribe()

    return
  }

  private GetNewUsername() {
    let username = 'User'
    let usrNumber = Math.floor(Math.random() * 10000000000).toString()
    return username.concat(usrNumber)
    // Should probably check if username is already taken.
  }

  private AuthenticateUser() {
    // -----------------------------------------------
    // TODO : Move this into the AUTH.SERVICE.TS file.
    // -----------------------------------------------
    // This is where we will authenticate the user...
    const usr = this.GetCurrentUser();
    usr.subscribe(result2 => {
      // -------------------------------------------
      // STEP 3: Get User's Nonce
      // -------------------------------------------
      this.nonce = result2.data['getUser'].nonce;
      // -------------------------------------------
      // STEP 4: Use Nonce & PublicKey to Sign a ETH Transaction
      // -------------------------------------------
      const sig = this.CreateSignature();
      sig.then(
        (data) => {
          this.signature = data;
        }
      )
        .then(
         () => {
           // -------------------------------------------
           // STEP 5: Get JSON Web Token and Store in Local Storage
           // -------------------------------------------
           const auth = this.AuthQuery();               // Call Authenticator from backend
           auth.pipe(
             map((res) => {
              this.jwt = res.data['authenticate']
             })
            )
             .subscribe(result => {
                localStorage.setItem('id_token', JSON.stringify(this.jwt));    // Save in local storage.
                // this.loading = false;
                console.log('local Storage set')
                console.log(localStorage.getItem('id_token'))
                this.router.navigateByUrl('/');
             })
          }
        )
        .catch(err => {
         console.error(err);
        })
        .finally(() => {
          this.loading = false;
       })
    })
  }
}
