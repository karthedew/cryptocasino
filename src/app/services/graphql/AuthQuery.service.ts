import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { User } from './UserQuery.service';

export interface Response {
    auth: string
}

@Injectable({
    providedIn: 'root'
})
export class AuthQueryGQL extends Query<Response> {
    document = gql`
    query AuthQueryGQL($publicAddress: String!, $signature: String!) {
        authenticate(publicAddress: $publicAddress, signature: $signature)
    }
    `;
}