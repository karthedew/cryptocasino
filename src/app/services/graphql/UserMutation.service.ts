import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';


export interface Response {
  createUser: Boolean;
}


@Injectable({
    providedIn: 'root',
  })
  export class GenerateUserGQL extends Query<Response> {
    // When wrapping a request in a GQL query, spaces matter.
    document = gql`
    mutation GenerateUserGQL($address: String!, $username: String!) {
      createUser(publickey: $address, username: $username)
    }
    `;
  }