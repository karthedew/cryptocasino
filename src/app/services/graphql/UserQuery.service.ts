import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';


@Injectable({
  providedIn: 'root',
})
export class AllUsersGQL extends Query<Response> {
  // When wrapping a request in a GQL query, spaces matter.
  document = gql`
  query {
    getUsers {
      id
      username
      publickey
      nonce
    }
  }
  `;
}


/*

The User interface service for querying a user from the backend.

*/
export interface User {
  id: number;
  username: string;
  publickey: string;
  nonce: number;
}

export interface Response {
  getUser: User;
}


@Injectable({
  providedIn: 'root',
})
export class UserQueryGQL extends Query<Response> {
  document = gql`
  query UserQueryGQL($publicAddress: String!) {
    getUser(address: $publicAddress) {
      id
      username
      publickey
      nonce
    }
  }
  `;
}

@Injectable({
  providedIn: 'root',
})
export class CheckUserGQL extends Query<any> {
  document = gql`
  query CheckUserGQL($publicAddress: String!) {
    checkUser(address: $publicAddress)
  }
  `;
}