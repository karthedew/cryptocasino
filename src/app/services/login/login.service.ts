import { InjectionToken, OnInit, Inject, Injectable } from "@angular/core";
import { WEB3 } from "../web3";
import Web3 from 'web3';

@Injectable({
    providedIn: 'root'
})
export class LoginService implements OnInit {

    // accounts: any;
    publicAddress: any;

    constructor(@Inject(WEB3) private web3: any) {}

    async ngOnInit() {
        if ('enable' in this.web3.currentProvider) {
            await this.web3.currentProvider.enable();
        }
        const accounts = await this.web3.eth.getAccounts();
        this.publicAddress = accounts[0];
        console.log(this.publicAddress);
    }

    async getPublicAddress() {

    }
}