import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// IMPORTED PAGES
import { AboutComponent } from './pages/about/about.component';
import { HomeComponent } from "./pages/home/home.component";
import { HowtoplayComponent } from "./pages/howtoplay/howtoplay.component";
import { LoginComponent } from './login/login.component';

// IMPORTED GUARDS
import { AuthGuardService as AuthGuard } from "./auth/guards/auth.guard";
// import { HomeGuard } from "./auth/guards/home.guard";


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: { title: 'Home Component' },
    canActivate:[AuthGuard]
  },
  { 
    path: 'about',
    component: AboutComponent,
    data: { title: 'About Component' },
    canActivate:[AuthGuard]
   },
  {
    path: 'howtoplay',
    component: HowtoplayComponent,
    data: { title: 'Contact Component' },
    canActivate:[AuthGuard]
  },
  { 
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login Component' }
   },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
