import { HttpClient } from '@angular/common/http';
import { WEB3 } from "../services/web3.service";
import { Inject, Injectable } from '@angular/core';
import Web3 from 'web3';
// import jwt from 'jsonwebtoken';
import { JwtHelperService } from "@auth0/angular-jwt";
import { Observable } from 'apollo-link';
import { BehaviorSubject } from 'rxjs';
import { User } from "../models/user.model";
// import { ReturnStatement } from '@angular/compiler';

@Injectable({
    providedIn: 'root'
})
export class AuthService {


    // Create Public & Private Variables
    public currentUser: Observable<User>
    private currentUserSubject: BehaviorSubject<User>

    constructor(
        public jwtHelper: JwtHelperService,
        @Inject(WEB3) private web3: Web3
    ) { }

    // ======================
    // --- PUBLIC METHODS ---
    // ======================

    public isAuthenticated(): boolean {
        
        if(localStorage.getItem('id_token')) {
            return true
        }
        return false
    }


    // Want to login with MetaMask

    public login(publicAddress: string, none: string) {

        // ============================================================
        // STEP 1: Query Backend for User with the right publicAddress
        // ============================================================
        // ============================================================
        // STEP 2: Verify digital signature.
        // ============================================================
        //  - STEP 2.1: throw new error if no instance of user. 
        //  - STEP 2.2: create signature message. 
        //  - STEP 2.3: We now are in possession of msg, publicAddress
        //              and signature. We will use a helper from
        //              eth-sig-util to extract the address from the
        //              signature.
        //  - STEP 2.4: The signature verification is successful if the
        //              address found with
        //              sigUtil.recoverPersonalSignature matches the
        //              initial publicAddress
        // ============================================================
        // STEP 3: Generate new nonce for the user.
        // ============================================================
        // ============================================================
        // STEP 4: Generate JWT for Authorized User.
        // ============================================================

    }

    logout() {
        localStorage.removeItem('id_token')
    }

    // =======================
    // --- PRIVATE METHODS ---
    // =======================

    private getUserBackend() {
        return("hello")
    }

    private storeJWTToken() {
        return("token")
    }

    private getDefaultUsername() {

        var userNumber, userName, name;
        var condition = false;
    
        // TODO : Need to check final userName is
        //        not already being used.
        
        while (!condition) {
            name = 'user';
            userNumber = Math.floor(Math.random() * 1000);
            userName = name.concat(userNumber.toString());
    
            condition = this.checkName(userName);
        }
        // name = 'user';
        // userNumber = Math.floor(Math.random() * 1000000);
        // userName = name.concat(userNumber.toString());
        
        return (userName)
    }
    
    private checkName(userName: string) {
    
        if (User.findOne(userName)) {
            return false
        }
    
        return true
    }

    // private doLoginUser(publicAddress: string, tokens: Tokens) {
    //     this.loggedUser = publicAddress;
    //     this.storeTokens(tokens);        
    // }

}







// export class AuthService {

//     private readonly JWT_TOKEN = 'JWT_TOKEN';
//     private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';
//     private loggedUser: string;

//     constructor(private http: HttpClient, @Inject(WEB3) private web3: Web3) { }


//     // --- PUBLIC METHODS ---
//     async login() {
//         // const publicAddress = web3.eth.coinbase.toLowerCase();
//         const coinbase = await this.web3.eth.getCoinbase();
//         const publicAddress = coinbase.toLowerCase();

//         // Check if user already exists.

//         // If user does not exist, create user.

//         // If user does exist, load user.
//     }

//     logout() {}

//     isLoggedIn() {}

//     refreshToken() {}

//     getJwtToken() {}

//     // --- PRIVATE METHODS ---
//     private doLoginUser(publicAddress: string, tokens: Tokens) {
//         this.loggedUser = publicAddress;
//         this.storeTokens(tokens);        
//     }

//     private doLogoutUser() {
        
//     }

//     private getRefreshToken() {
        
//     }

//     private storeJwtToken(jwt: string) {
        
//     }

//     private storeTokens(tokens: Tokens) {
//         localStorage.setItem(this.JWT_TOKEN, tokens.jwt);
//         localStorage.setItem(this.REFRESH_TOKEN, tokens.refreshToken);
//     }

//     private clearTokens() {
        
//     }
// }