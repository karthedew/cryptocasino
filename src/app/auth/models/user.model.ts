import { BaseEntity } from "typeorm";

export class User extends BaseEntity {
    public id!: number;             // note that the 'null assertion' '!' is required in strict mode.
    public nonce!: number;
    public publicAddress!: string;
    public username?: string;       // for nullable fields
}