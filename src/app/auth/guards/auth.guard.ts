import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AuthService } from "../services/auth.service";


/*
This Authorization Gaurd can be called to restrict access to an HTTP 
endpoint and redirect the user to the "/login" route.
*/


@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

    constructor(
        private authService: AuthService,
        private router: Router
        ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.authService.isAuthenticated()) {
            return true;
        }
        // Re-direct to the LOGIN page and return false
        this.router.navigate(['login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}