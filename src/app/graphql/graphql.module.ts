import {NgModule} from '@angular/core';
import {ApolloModule, APOLLO_OPTIONS} from 'apollo-angular';
import {HttpLinkModule, HttpLink} from 'apollo-angular-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import { ApolloBoostModule, ApolloBoost } from "apollo-angular-boost";
import { HttpClientModule } from '@angular/common/http';

// ========================================================================================================
// TODO: Create a GraphQL server and add the URL.
// --- Create a GraphQL Server Here ---
// const uri = ''; // <-- add the URL of the GraphQL server here
// const uri = 'https://o5x5jzoo7z.sse.codesandbox.io/graphql'; //our test Graphql Server which returns rates\
// const uri = 'http://localhost:4000/graphql'; //our test Graphql Server which returns rates\
// ========================================================================================================
// export function createApollo(httpLink: HttpLink) {
//   return {
//     link: httpLink.create({uri}),
//     cache: new InMemoryCache(),
//   };
// }

@NgModule({
  exports: [
    ApolloModule,
    HttpLinkModule,
    HttpClientModule,
    ApolloBoostModule
  ]
  // providers: [
  //   {
  //     provide: APOLLO_OPTIONS,
  //     useFactory: createApollo,
  //     deps: [HttpLink],
  //   },
  // ],
})
export class GraphQLModule {
  constructor(
    apolloBoost: ApolloBoost
  ) {
    apolloBoost.create({
      uri: "http://localhost:4000/graphql"
    })
  }
}
