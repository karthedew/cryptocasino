import gql from 'graphql-tag';


// ================================
// --- GRAPHQL MODELS 4 QUERIES ---
// ================================

export const AllUsers = gql`
  query AllUsers {
      getUsers {
          id
          username
          publicKey
          email
      }
  }
`
export const User = gql`
  query User {
      getUser {
          id
          username
          publicKey
          email
      }
  }
`

// ==================================
// --- GRAPHQL MODELS 4 MUTATIONS ---
// ==================================