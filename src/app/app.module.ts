import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// --- FLEXBOX ---
// import { FlexLayoutModule } from '@angular/flex-layout';

// --- ANGULAR MATERIALS ---
import { MatToolbarModule } from "@angular/material/toolbar";
import { CustomMaterialModule } from "./materials/materials.module";
import { MatCardModule } from "@angular/material/card";

// --- JWT MODULES ---
import { JwtModule } from "@auth0/angular-jwt";

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// --- IMPORT PAGES ---
import { HowtoplayComponent } from './pages/howtoplay/howtoplay.component';
import { AboutComponent } from './pages/about/about.component';
import { HomeComponent } from './pages/home/home.component';

// --- IMPORT ROUTING ---
import { AppRoutingModule } from './app-routing.module';
import { GraphQLModule } from './graphql/graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';


export function tokenGetter() {
  return localStorage.getItem("access_token");
}


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HowtoplayComponent,
    AboutComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    // FlexLayoutModule,
    MatCardModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    CustomMaterialModule,
    GraphQLModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ["localhost:4200"],
        blacklistedRoutes: [""]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
